//
//  LinkLocation.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation

class LinkLocation {
    
    var  country = LinkCountry()
    
    var name :String = ""
    
    func setLinkLocation(location:[String : Any]){
        if let val = location["country"] {
        country.setLinkCountry(countryInfo: val as! [String : Any])
        }
        if let val = location["name"] {
        self.name = val as! String
        }
    }
    
}
