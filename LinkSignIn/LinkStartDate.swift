//
//  LinkStartDate.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation

class LinkStartDate{
    var month : Int = 0
    var year : Int = 0
    func setStartDate(date:[String:Any]){
        if let val = date["month"] {
        self.month = val as! Int
        }
         if let val = date["year"] {
        self.year = val as! Int
        }
    }
}
