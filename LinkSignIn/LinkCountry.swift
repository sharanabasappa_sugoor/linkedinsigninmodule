//
//  LinkCountry.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
class LinkCountry {
    var code : String = ""
    var name : String = ""
    
    func setLinkCountry(countryInfo : [String:Any]){
        if let val =  countryInfo["code"]  {
             self.code = val as! String
        }
        if let val =  countryInfo["name"] {
            self.name = val as! String
        }
    }
}
