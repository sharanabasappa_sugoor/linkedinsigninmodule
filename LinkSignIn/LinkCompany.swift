//
//  LinkCompany.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
class LinkCompany {
    
    var id : Int = 0
    var industry : String = ""
    var name : String = ""
    var size : String = ""
    var type : String = ""
    
    func setCoMpanyInfo(companyInfo : [String:Any]){
        if let val = companyInfo["id"] {
            self.id = val as! Int
        }
        if let val = companyInfo["industry"] {
            self.industry = val as! String
            
        }
        if let val = companyInfo["name"] {
            self.name = val as! String
        }
        if let val = companyInfo["size"] {
            self.size =    val as! String
        }
        if let val = companyInfo["type"] {
            self.type =   val as! String
        }
    }
}
