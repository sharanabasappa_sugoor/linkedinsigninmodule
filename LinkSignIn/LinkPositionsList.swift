//
//  LinkPositionsList.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
class LinkPositionsList{
    var id : Int = 0
    var isCurrent : Bool = false
    var location = LinkLocation()
    var startDate = LinkStartDate()
    var title : String  = ""
    
    func setPositionsInfo(position : [String:Any]){
        if let id = position["id"] {
        self.id = id as! Int
        }
        if let isCurrentLoc =  position["isCurrent"]  {
        self.isCurrent = isCurrentLoc  as! Bool
        }
        if let title_Loca =  position["title"] {
        self.title = title_Loca as! String
        }
         if let Loca =  position["location"]  {
        self.location.setLinkLocation(location:Loca as! [String : Any])
        }
        if let joiningDate = position["startDate"]{
        self.startDate.setStartDate(date:joiningDate as! [String : Any])
        }
     
    }
    
}
