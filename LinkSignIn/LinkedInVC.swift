//
//  ViewController.swift
//  LinkSignIn
//
//  Created by Fresher on 07/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import UIKit

class LinkedInVC: UIViewController {
    var window: UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        
        LinkedInManager.CreateLoginSeesion { (success, error) in
            if success{
                LinkedInManager.getContactsInformation(successClosure: { (success, erroe) in
                    if success{
                        //success block
                    }
                    else{
                        
                    }
                })
            }else{
                
            }
        }
        // LinkedInManager.getCompleteInformation()
        
    }
    
    
    
    @IBAction func shareAction(_ sender: Any) {
        LinkedInManager.shareInfo { (success, error) in
            
            if success {
                self.alertFor(alertString :"Posted Success fully")
            }
            else{
                self.alertFor(alertString :"Sharing a Post Failed")
            }
        }
    }
    
    func alertFor(alertString :String){
        let alert = UIAlertController(title: "LinkSignIn", message:alertString,  preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        let homeController = self.navigationController?.visibleViewController
        homeController?.present(alert, animated: true, completion: nil)
    }
}
