//
//  LinkPositions.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
class  LinkPositions {
    var total : Int = 0
    var values : [Any] = []
    var lists :[LinkPositionsList] = []
    var value = LinkPositionsList()
    func setEachPositionsInfo(positions:[String:Any]){
        if let total_Val =  positions["_total"] {
            self.total = total_Val as! Int
        }
        if let pos_Values = positions["values"]  {
        for val in (pos_Values as? [Any])! {
            value.setPositionsInfo(position: val as! [String : Any])
           
            self.lists.append(value)
        }
           
        }
        
    }
    
}
