//
//  LinkShareModel.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
class LinkShareModel {

    var comment : String = ""
    var content = LinkShareContent()
    var visibility = LinkVisiblity()
    

    func getShareContent()->[String:Any]{
      
        let contentInfo = ["comment": comment,"code":visibility.code,"title" :content.title,"description": content.desciption,"submitted-url": content.submitted_url, "submitted-image-url":content.submitted_image_url]
   
        return contentInfo
    }
    
}
