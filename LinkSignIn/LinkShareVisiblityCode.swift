//
//  LinkShareVisiblityCode.swift
//  LinkSignIn
//
//  Created by Fresher on 10/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
enum Visiblity : String {
    case Anyone = "anyone"
    case Connections = "connections-only"
}
class LinkVisiblity{
    var  code  = Visiblity.Connections.rawValue
}
