//
//  LinkidInBasicInfo.swift
//  LinkSignIn
//
//  Created by Fresher on 07/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
class LinkidInBasicinfo{
    static let sharedInstance: LinkidInBasicinfo = {
        let instance = LinkidInBasicinfo()
        
        // setup code
        
        return instance
    }()
    /*
     {
     "firstName": "Frodo",
     "headline": "2nd Generation Adventurer",
     "id": "1R2RtA",
     "lastName": "Baggins",
     "siteStandardProfileRequest": {
     "url": "https://www.linkedin.com/profile/view?id=…"
     }
     }
     */
    /*
     let shareInfoList = ["id","first-name","last-name","maiden-name","formatted-name","phonetic-first-name","phonetic-last-name","headline","location","industry","current-share","num-connections","num-connections-capped","summary","specialties","picture-url","picture-urls::(original)","site-standard-profile-request","api-standard-profile-request","api-standard-profile-request","email-address"]
     */
    
    
    var id :String = ""
    var firstName : String = ""
    var lastname : String = ""
    var maiden_name : String = ""
    var formatted_name :String = ""
    var phonetic_first_name :String = ""
    var phonetic_last_name : String = ""
    var headline : String = ""
    var location =  LinkLocation()
    var num_connections : Int = 0
    var num_connections_capped : Bool = false
    var summary : String = ""
    var positions = LinkPositions()
    var picture_url : String = ""
    var original_Pic_url = linkProfileURL()
    var site_standard_profile_request = LinkSiteStandardPFRequest()
    var api_standard_profile_request : String = ""
    var siteStandardProfileRequest = LinkSiteStandardPFRequest()
    var industry :String = ""
    
    func setBasicProfileInfo(basicInfo:[String:Any]){
        self.firstName = basicInfo["firstName"] as! String
        self.lastname = basicInfo["lastName"] as! String
        self.headline = basicInfo["headline"] as! String
        self.id = basicInfo["id"] as! String
        self.siteStandardProfileRequest.siteStandardPFRequest(siteStandardProfileRequest:  (basicInfo["siteStandardProfileRequest"] as! [String : Any]?)!)
        
    }
    
    
    func setCompleteInfo(basicInfo:[String:Any]){
        
        self.firstName = LinkedInManager.checkFornil(value: basicInfo["firstName"] ) as! String
        
        
        self.lastname = LinkedInManager.checkFornil(value: basicInfo["lastName"] ) as! String
        
        self.headline = LinkedInManager.checkFornil(value: basicInfo["headline"] ) as! String
        self.id = LinkedInManager.checkFornil(value:   basicInfo["id"] ) as! String
        
        self.maiden_name = LinkedInManager.checkFornil(value: basicInfo["maidenName"] ) as! String
        
        self.formatted_name =  LinkedInManager.checkFornil(value:basicInfo["formattedName"] ) as! String
        self.industry =  LinkedInManager.checkFornil(value:basicInfo["industry"] ) as! String
        
        self.phonetic_first_name =  LinkedInManager.checkFornil(value: basicInfo["phoneticFirstName"] ) as! String
        
        self.phonetic_last_name = LinkedInManager.checkFornil(value: basicInfo["phoneticLastName"] ) as! String
        
        if let loc = basicInfo["location"]  {
        self.location.setLinkLocation(location:loc as! [String : Any])
        }
        if let numConn =  basicInfo["numConnections"] {
        self.num_connections = numConn as! Int
        }
        if let val = basicInfo["numConnectionsCapped"] {
        self.num_connections_capped = val as! Bool
        }
        
        self.summary = LinkedInManager.checkFornil(value: basicInfo["summary"] ) as! String
         if let pos = basicInfo["positions"]  {
        self.positions.setEachPositionsInfo(positions: pos as! [String : Any])
        }
          if let Purl = basicInfo["pictureUrl"]  {
        self.picture_url = LinkedInManager.checkFornil(value: Purl) as! String
        }
         if let pOriginal = basicInfo["pictureUrls"]  {
        self.original_Pic_url.setLinkPictureUrl(pictureUrl:pOriginal as! [String : Any])
        }
         if let siteUrl = basicInfo["siteStandardProfileRequestUrl"]  {
        self.site_standard_profile_request.siteStandardPFRequest(siteStandardProfileRequest:  siteUrl as! [String : Any])
        }
        self.api_standard_profile_request = LinkedInManager.checkFornil(value: basicInfo["apiStandardProfileRequestUrl"] ) as! String
         if let siteStandardPR = basicInfo["siteStandardProfileRequest"]  {
        self.siteStandardProfileRequest.siteStandardPFRequest(siteStandardProfileRequest:  siteStandardPR as! [String : Any])
        }
    }
    
}
