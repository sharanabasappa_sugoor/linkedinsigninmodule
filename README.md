# README #

This module allows you to  login  and Sharing post .

### What is this repository for? ###

* [Sign in With LinkedIn](https://developer.linkedin.com/docs/signin-with-linkedin)
* [Share on LinkedIn](https://developer.linkedin.com/docs/share-on-linkedin)
* [Linkedin Docs](https://developer.linkedin.com/docs)
### Configuring linkedin FrameWork
#### Step1 :  
<key>LIAppId</key>
<string>{App id}</string>

<key>CFBundleURLTypes</key>
<array>
<dict>
<key>CFBundleURLSchemes</key>
<array>
<string>li{App id}</string>
</array>
</dict>
</array>
<key>LSApplicationQueriesSchemes</key>
<array>
<string>linkedin</string>
<string>linkedin-sdk2</string>
<string>linkedin-sdk</string>
</array>

<key>NSAppTransportSecurity</key>
<dict>
<key>NSExceptionDomains</key>
<dict>
<key>linkedin.com</key>
<dict>
<key>NSExceptionAllowsInsecureHTTPLoads</key>
<true/>
<key>NSIncludesSubdomains</key>
<true/>
<key>NSExceptionRequiresForwardSecrecy</key>
<false/>
</dict>
</dict>
</dict>
* add this to info.plist
####  Step2 : Create Bridge by header.h file  
* Add Following Code
* "#ifndef Header_h #define Header_h #import <UIKit/UIKit.h>* #import <Foundation/Foundation.h> #import <linkedin-sdk/LISDK.h>"
 #endif /* Header_h */"

#### step3 Configuring the Bridge header file  
* Go to Build Setting -> Objcective C Bridging header -> Add File name Header.h
* Go to Build Setting -> PreComplie Prefix header -> Set it True




### Information Shaed on linkedin ###
* [Basic profile Information ](https://developer.linkedin.com/docs/fields/basic-profile)
* Share Post


### Api guidelines ###

### Basic Profile Information###
* [Basic profile Information Api URL](https://api.linkedin.com/v1/people/~?format=json)

*   Request url :  https://api.linkedin.com/v1/people/~?format=json
*   Response:

* {   
"firstName": "Frodo",
"headline": "2nd Generation Adventurer",
"id": "1R2RtA",
"lastName": "Baggins",
"siteStandardProfileRequest": {
"url": "https://www.linkedin.com/profile/view?id=…"
}
}

### Complete Profile Information ###

*   parameters : "id,num-connections,picture-url"

*   Request URL : https://api.linkedin.com/v1/people/~:(parameters)?format=json
*   [Diffrent paramaters listed Here.](https://developer.linkedin.com/docs/fields/basic-profile)
*   Response: It's based on the Parameter that you pass.

### Share Post  ###
*  Request URL : https://api.linkedin.com/v1/people/~/shares?format=json
*  Request Body :
*   {
"comment": "Check out developer.linkedin.com!",
"content": {
"title": "LinkedIn Developers Resources",
"description": "Leverage LinkedIn's APIs to maximize engagement",
"submitted-url": "https://developer.linkedin.com",  
"submitted-image-url": "https://example.com/logo.png"
},
"visibility": {
"code": "anyone"
}  
}

